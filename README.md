# MineField

## Explore the nodes and find explosives.

**Rules**
- Click on a node to explore it.
- Don't click on nodes that can potentially contain explosives, otherwise, it is game over

**Hints**
- Nodes that are adjacents to the node being explored will be automatically explored and reveal their content 
- If a node being explored has an explosive in any of its second degree neighbour, the automatic exploration will stop on that node.

**Characteristics**
- Multiple levels with more or less complex nodes structures.
- Increased number of explosives with difficulty level.
- 9 levels in total

**Level 1**
![Level 1](./Resources/Level 1.gif)

**Level 2**
![Level 2](./Resources/Level 2.gif)

**Level 3**
![Level 3](./Resources/Level 3.gif)

**Level 8**
![Level 8](./Resources/Level 8.gif)

**Level 9**
![Level 9](./Resources/Level 9.gif)