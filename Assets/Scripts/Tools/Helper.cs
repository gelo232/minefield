﻿using KGW.DataStructures.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace KGW.DataStructures.Tools
{
    public static class Helper
    {
        public static T FindObjectOfType<T>(bool includeInactive) where T : Component
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                foreach (var root in scene.GetRootGameObjects())
                {
                    var res = root.GetComponentInChildren<T>(includeInactive);
                    if (res != null)
                        return res;
                }
            }

            return null;
        }
    }
}