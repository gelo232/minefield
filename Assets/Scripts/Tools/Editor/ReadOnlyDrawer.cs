﻿using UnityEditor;
using UnityEngine;

namespace KGW.Tools
{
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(property, label);
            EditorGUI.EndDisabledGroup();
        }
    }
}