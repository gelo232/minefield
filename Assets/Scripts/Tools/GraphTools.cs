﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using KGW.DataStructures;
using Random = UnityEngine.Random;

namespace KGW.DataStructures.Tools
{
    /// <summary>
    /// A set of tools to generate the graph. This includes the Graph Vertices generator as well as the Edges generator.
    /// </summary>
    public static class GraphTools
    {
        public static List<N> GenerateVertices<N>(int verticesCount) where N: GraphNode<N>
        {
            List<N> graphVertices = new List<N>();
            for (int i = 0; i < verticesCount; i++)
                graphVertices.Add(new GameObject("Node "+i).AddComponent<N>());
            return graphVertices;
        }

        private static List<E> ConnectVertices<E, N>(List<N> vertices, Action<N, N> connectCallback = null) where E : GraphEdge<N>, new() where N : GraphNode<N>
        {
            List<E> graphEdges = new List<E>();
            if (vertices.Count < 2) return graphEdges;

            for (int i = 0; i < vertices.Count; i++)
            {
                var nodeA = vertices[i];
                var nodeB = vertices[(i + 1) % vertices.Count];

                var e = new E();
                e.SetNodes(nodeA, nodeB);// Here, nodeB automatically becomes nodeA's neighbor
                graphEdges.Add(e);

                connectCallback?.Invoke(nodeA, nodeB);
            }

            return graphEdges;
        }
        
        private static E ConnectNodes<E, N>(N nodeA, N nodeB) where E : GraphEdge<N>, new() where N : GraphNode<N>
        {
            var e = new E();
            e.SetNodes(nodeA, nodeB);
            return e;
        }

        /// <summary>
        /// Constructs the edges between the given list of vertices
        /// </summary>
        /// <typeparam name="E"></typeparam>
        /// <typeparam name="N"></typeparam>
        /// <param name="vertices">The list of vertices</param>
        /// <param name="connectionsCount">The desired number of connection per vertices.</param>
        /// <param name="facesDegree">The desired number of vertices per regular polygons.</param>
        /// <returns></returns>
        public static List<E> GenerateEdges<E, N>(List<N> vertices, int connectionsCount, int facesDegree) where E : GraphEdge<N>, new() where N : GraphNode<N>
        {
            List<E> graphEdges = new List<E>();
            List<E> lastBoundingEdges = new List<E>();

            // Create the outer face of the graph
            var outerFaceVertices = vertices.GetRange(0, facesDegree);
            var otherVertices = vertices.Skip(outerFaceVertices.Count);
            graphEdges.AddRange(
                lastBoundingEdges = ConnectVertices<E, N>(
                    outerFaceVertices,
                    (a, b) => {
                        a.IsOuterNode = b.IsOuterNode = true;
                    }
                ));

            // Ensure that there are more vertices than connections in order to maximize the chances to get the same number of connections per vertex
            connectionsCount = Math.Min(connectionsCount, vertices.Count - 1);

            var previousVertices = outerFaceVertices;
            var innerFacesVertices = new List<N>();

            // First, extract facesDegree vertices from the inner vertices to build the first inner polygon
            if(otherVertices.Count() >= facesDegree)
            {
                innerFacesVertices = otherVertices.Take(facesDegree).ToList();
                otherVertices = otherVertices.Skip(facesDegree);
            }
            else
            {
                innerFacesVertices = otherVertices.ToList();
            }

            // While there are still enough vertices to build a polygon of facesDegree degree
            while (innerFacesVertices.Count >= facesDegree)
            {
                var shouldLinkVertices = false;

                // Link every vertex of the current inner polygon to the previous polygon
                for(int i = 0; i < facesDegree; i++)
                {
                    var node = previousVertices[i]; // Let node be the i'th vertex of the previous polygon
                    var v = innerFacesVertices[i]; // Let v be the i'th vertex of the current polygon
                    // If node is not fully satisfied in terms of number of connection, then we link it to the v
                    if (node.NeighborsCount < connectionsCount)
                    {
                        shouldLinkVertices = true; // It will be necessary to link all the vertices of the current polygon together in order to satisfy the connection count
                        graphEdges.Add(ConnectNodes<E, N>(node, v)); // Here we connect node to v
                    }
                    // If node has enough connection, then it will be necessary to insert v into the ith edge of the previous polygon
                    else
                    { 
                        var faceEdge = lastBoundingEdges[i]; // Let faceEdge be the i'th edge of the previous polygon
                        graphEdges.Remove(faceEdge); // Here we remove the edge from the edge list (since the edge will not exist anymore
                        faceEdge.Split(); // Here we split the edge to allow insertion
                        // Then we insert v into faceEdge, by connecting its bounding vertices to v
                        graphEdges.Add(ConnectNodes<E, N>(faceEdge.NodeA, v));
                        graphEdges.Add(ConnectNodes<E, N>(faceEdge.NodeB, v));

                        // Link the new vertices together if there is not enough remaining vertices to satisfy the face's degree constraint
                        shouldLinkVertices = otherVertices.Count() < facesDegree;
                    }
                }

                // Link the vertices of the current polygon together if necessary
                if(shouldLinkVertices)
                {
                    var currentBoundingEdges = ConnectVertices<E, N>(innerFacesVertices);
                    graphEdges.AddRange(currentBoundingEdges);
                    lastBoundingEdges = currentBoundingEdges;
                }

                // Keep the track on the current polygon to be used in the next loop iteration
                previousVertices = innerFacesVertices;
                if (otherVertices.Count() >= facesDegree)
                {
                    innerFacesVertices = otherVertices.Take(facesDegree).ToList();
                    otherVertices = otherVertices.Skip(facesDegree);
                }
                else
                    innerFacesVertices = otherVertices.ToList();
            }

            // For the other remaining vertices, that's to say the vertices that cannot construct a regular polygon of degree facesDegree, we will connect them to the previous polygon
            var K = otherVertices.Count(); // Let K be the number of such vertices
            graphEdges.AddRange(ConnectVertices<E, N>(otherVertices.ToList()));
            var remainingVertices = otherVertices.ToList();
            int edgeInd = 0;
            var freeVertices = new Queue<N>();
            // While there is a vertex that isn't satisfied in terms of number of connections
            while (remainingVertices.Any(v => v.NeighborsCount < connectionsCount))
            {
                var incomingVertex = remainingVertices.First(); // Let incomingVertex be the vertex that we want to connect to the previous polygon
                // We will firstly try to satisfy the connection constraint by connecting the vertex to the free available vertices from the previous polygon (that's those which are not satisfied anymore because of a vertex insertion
                while (freeVertices.Count > 0 && incomingVertex.NeighborsCount < connectionsCount)
                    graphEdges.Add(ConnectNodes<E, N>(freeVertices.Dequeue(), incomingVertex));

                // If the vertex is still not satisfied, therefore we will connect it to the other vertices from the previous polygon, by inserting it into edges
                while (incomingVertex.NeighborsCount < connectionsCount)
                {
                    var outgoingEdge = lastBoundingEdges[edgeInd]; // Let outgoingEdge be the edge that we want to split to insert the vertex
                    edgeInd += 2;// Random.Range(1, 2); // This is used to prioritize a nice looking graph

                    // If K is odd, then at least one vertex of the graph will be unsatisfied. We will the ensure to keep a nice look. Therefore no edge will be split
                    if (K % 2 == 1 && remainingVertices.Count == 1)
                    {
                        graphEdges.Add(ConnectNodes<E, N>(outgoingEdge.NodeB, incomingVertex)); // here we connect the vertex to the last end of the edge
                        if(incomingVertex.NeighborsCount < connectionsCount) // If by performing such a connection, the vertex is still unsatisfied, then we will connect it to the other end of the edge (even if this case should normally not happen)
                            graphEdges.Add(ConnectNodes<E, N>(outgoingEdge.NodeA, incomingVertex));
                    }
                    // If K is even, then we will directly connect the vertex to both ends of the edge once split, since no vertex will remain unsatisfied
                    else
                    {
                        // Remove the edge and split it if its ends are already satisfied
                        if(outgoingEdge.NodeA.NeighborsCount >= connectionsCount)
                        {
                            graphEdges.Remove(outgoingEdge);
                            outgoingEdge.Split();
                        }

                        graphEdges.Add(ConnectNodes<E, N>(outgoingEdge.NodeA, incomingVertex)); // Create a connection with the vertex to the first end of the edge
                        
                        if (incomingVertex.NeighborsCount < connectionsCount) // If the vertex is not satisfied, then connect it to the other end
                            graphEdges.Add(ConnectNodes<E, N>(outgoingEdge.NodeB, incomingVertex));
                        else // Otherwise, mark the other end as a free available node, since it is not satisfied anymore, because of the previous edge split
                            freeVertices.Enqueue(outgoingEdge.NodeB);
                    }
                }

                // Finally, remove the vertex from the awaiting vertices list, since it is already satisfied
                remainingVertices.Remove(incomingVertex);
            }

            // Calculate the distance value of each node, that means the distance from each node to the outer nodes
            CaculateDistancesToBoundaries<E, N>(outerFaceVertices);

            // Now everything is done. Return the result graph edges
            return graphEdges;
        }

        private static void CaculateDistancesToBoundaries<E, N>(List<N> outerFaceVertices) where E : GraphEdge<N>, new() where N : GraphNode<N>
        {
            var nodesToVisit = new List<N>(outerFaceVertices);
            var currentLevel = 0; // The level represents the node distance to the outer vertices. Since we start with the outer vertices themselves, therefore the inital distance is zero
            while (nodesToVisit.Count > 0) // While there is a node to visit
            {
                var nextNodesToVisit = new List<N>();
                // Loop through all nodes to visit and set the current level as the distance value for that node
                foreach (var node in nodesToVisit)
                {
                    node.isExplored = true; // Mark the node as explored to prevent cycling when exploring connected nodes
                    node.DistanceToBoundary = currentLevel;
                }

                // Add the unexplored nodes that are children of the nodes that we just explored, to the list of the nex nodes to visit
                foreach (var node in nodesToVisit)
                    nextNodesToVisit.AddRange(node.Neighbors.Where(n => !n.isExplored).Select(n => (N) n));

                // Save the list to update the loop condition
                nodesToVisit = nextNodesToVisit;
                // Increase the current level since we will go deeper in the next loop iteration
                currentLevel++;
            }
        }

        /// <summary>
        /// Calculates the cooling value depending on the iteration and the number of vertices
        /// </summary>
        /// <param name="i">The current iteration</param>
        /// <param name="n">The number of vertices that make the graph</param>
        /// <returns>The cooling value that will prevent nodes from moving faster.</returns>
        public static float Cool(int i, float areaPerVertex) =>
            Mathf.Sqrt(areaPerVertex) / (1 + areaPerVertex * Mathf.Sqrt(i * i * i));
        
        /// <summary>
        /// Computes the influence of 2 vertices due to the difference of distances from both vertices to the outer vertices
        /// </summary>
        /// <typeparam name="N"></typeparam>
        /// <param name="A">The first vertex</param>
        /// <param name="B">The second vertex</param>
        /// <param name="modulator">The modulator factor to temper the result</param>
        /// <param name="maxPeriphericity">The maximum distance from a node of the graph to the outer vertices</param>
        /// <returns></returns>
        public static float PeriphericInfluence<N>(N A, N B, float modulator, float maxPeriphericity) where N : GraphNode<N>
        {
            return Mathf.Exp(-modulator * (2 * maxPeriphericity - A.DistanceToBoundary - B.DistanceToBoundary) / maxPeriphericity);
        }
    }
}