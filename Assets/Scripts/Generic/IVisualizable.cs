﻿using UnityEditor;
using UnityEngine;

namespace KGW.DataStructures.Generic
{
    public interface IVisualizable
    {
        void Render(GameObject template, Transform container, float renderersScale);
        void UpdateScale(Vector3 scale);
    }
}