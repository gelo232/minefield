﻿using KGW.DataStructures.Tools;
using UnityEngine;

namespace KGW.DataStructures.Generic
{
    public class Singleton<T> : MonoBehaviour where T:Component
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null && (_instance = Helper.FindObjectOfType<T>(true)) == null)
                    _instance = new GameObject("Graph Manager").AddComponent<T>();
                return _instance;
            }
        }
    }
}