﻿using System;
using System.Collections.Generic;
using System.Linq;
using KGW.DataStructures.Generic;
using KGW.DataStructures.Tools;
using UnityEngine;
using Random = UnityEngine.Random;

namespace KGW.DataStructures
{
    public abstract class GraphManager<M, E, N> : Singleton<M> where M : GraphManager<M, E, N> where E : GraphEdge<N>, new() where N:GraphNode<N>
    {
        /// <summary>
        /// The vertex degree is 3, in order to build a 3-connected graph
        /// </summary>
        public const int VERTEX_DEGREE = 3;

        [Range(3, 100)]
        public int VerticesCount = 10;
        [Range(1, 10)]
        public int ConnectionCount = VERTEX_DEGREE;
        [Range(1, 500)]
        public int IterationCount = 10;
        [Range(3, 20)]
        public int PolyhedralOrder = 5;

        public bool RandomPolyhedralOrder;
        [Range(0, 10)]
        public float PeriphericityModulator = 2.5f;

        [Range(1, 100)]
        public float GraphAreaRadius = 5;

        [Range(0, 1)]
        public float MaxVerticeHeight = .5f;
        
        protected List<N> GraphNodes;
        protected List<E> GraphEdges;

        protected virtual void Awake()
        {
            ConnectionCount = VERTEX_DEGREE;
        }

        /// <summary>
        /// Build the graph vertices and edges.
        /// The generated graph is polyhedral graph, which is by definition planar and connected.
        /// </summary>
        public virtual void BuildGraph()
        {
            if (RandomPolyhedralOrder) PolyhedralOrder = Random.Range(3, 21);
            GraphNodes = GraphTools.GenerateVertices<N>(VerticesCount);
            GraphEdges = GraphTools.GenerateEdges<E, N>(GraphNodes, ConnectionCount, PolyhedralOrder);
        }

        public void Cleanup()
        {
            if(GraphNodes != null)
            {
                foreach (var node in GraphNodes)
                {
                    node.Cleanup();
                    DestroyImmediate(node.gameObject);
                }
            }

            if(GraphEdges != null)
            {
                foreach (var edge in GraphEdges)
                    edge.Cleanup();
            }

            GraphNodes?.Clear();
            GraphEdges?.Clear();
        }
    }
}