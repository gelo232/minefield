﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KGW.DataStructures
{
    public abstract class GraphNode<N>:MonoBehaviour where N:GraphNode<N>
    {
        private List<N> neighbors = new List<N>();
        protected Dictionary<N, GraphEdge<N>> adjacentEdges = new Dictionary<N, GraphEdge<N>>();
        public IEnumerable<N> Neighbors => neighbors;
        public int NeighborsCount => neighbors.Count;
        public Vector3 Force { get; set; }
        public int DistanceToBoundary { get; set; }

        public bool isExplored { get; set; }
        
        public abstract Vector3 Position { get; }
        public bool IsOuterNode { get; set; }

        public abstract void SetPosition(Vector3 newPos);
        public virtual void NotifyMineRevealed(N node){}
        
        public void AddNeighbor(N node, GraphEdge<N> edge)
        {
            if(!neighbors.Contains(node))
                neighbors.Add(node);
            
            if (!node.neighbors.Contains(this))
                node.neighbors.Add((N)this);

            if(!adjacentEdges.ContainsKey(node))
                adjacentEdges.Add(node, edge);

            if(!node.adjacentEdges.ContainsKey((N)this))
                node.adjacentEdges.Add((N)this, edge);
        }

        public void RemoveNeighbor(N node)
        {
            if (neighbors.Contains(node))
                neighbors.Remove(node);

            if (node.neighbors.Contains(this))
                node.neighbors.Remove((N)this);

            if (adjacentEdges.ContainsKey(node))
                adjacentEdges.Remove(node);

            if (node.adjacentEdges.ContainsKey((N)this))
                node.adjacentEdges.Remove((N)this);
        }

        internal virtual void Cleanup()
        {
            neighbors.Clear();
            adjacentEdges.Clear();
        }

        internal bool IsNeighborOf(N node) => neighbors.Contains(node);
    }
}