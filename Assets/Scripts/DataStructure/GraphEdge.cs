﻿using System;

namespace KGW.DataStructures
{
    public class GraphEdge<T> where T: GraphNode<T>
    {
        public T NodeA { get; private set; }
        public T NodeB { get; private set; }
        
        public GraphEdge(){}

        protected GraphEdge(T a, T b)
        {
            SetNodes(a, b);
        }

        public void SetNodes(T nodeA, T nodeB)
        {
            NodeA = nodeA;
            NodeB = nodeB;
            NodeA.AddNeighbor(NodeB, this);
        }

        public void Split()
        {
            NodeA.RemoveNeighbor(NodeB);
        }

        internal virtual void Cleanup()
        {
            NodeA = NodeB = null;
        }
    }
}