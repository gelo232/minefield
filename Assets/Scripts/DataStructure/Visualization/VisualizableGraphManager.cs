﻿using System;
using System.Collections.Generic;
using System.Linq;
using KGW.DataStructures;
using KGW.DataStructures.Tools;
using UnityEngine;
using Random = UnityEngine.Random;

namespace KGW.MineField
{
    public abstract class VisualizableGraphManager<G, E, N> : GraphManager<G, E, N>
        where G : VisualizableGraphManager<G, E, N> 
        where E : VisualizableGraphEdge<N>, new()
        where N : VisualizableGraphNode<N>
    {

        [Range(0, 100)]
        public int Seed;
        [Range(0, 10)]
        public float RenderersScale = 1;
        [Range(.5f, 1)]
        public float AreaResolution = .75f;

        [Range(0, 10)]
        public float Repulsion = 0;
        [Range(.1f, 100)]
        public float Attraction = 1;

        public GameObject NodeRenderer;
        public GameObject EdgeRenderer;
        private Rect screenSafeArea => Screen.safeArea;

        public Transform NodesContainer { get; private set; }
        public Transform EdgesContainer { get; private set; }
        

        protected virtual void Start()
        {
            //screenSafeArea = Screen.safeArea;
            NodesContainer = new GameObject("Nodes").transform;
            NodesContainer.SetParent(transform);
            NodesContainer.localPosition = Vector3.zero;

            EdgesContainer = new GameObject("Edges").transform;
            EdgesContainer.SetParent(transform);
            EdgesContainer.localPosition = Vector3.zero;
        }

        public void VisualizeGraph()
        {
            DistributeNodesPositions();
            Render();
        }

        /// <summary>
        /// Computes the attractive force exerted by A on B, meaning that B is pulled to A
        /// </summary>
        /// <param name="A">The attracting node</param>
        /// <param name="B">The attracted node</param>
        /// <param name="k">The modulator</param>
        /// <returns></returns>
        private Vector3 AttractiveForce(N A, N B, float k)
        {
            var d = A.Position - B.Position;
            return d.normalized * d.sqrMagnitude / k;
        }

        /// <summary>
        /// Computes the repulsive force exerted by A on B, meaning that B is pushed from A
        /// </summary>
        /// <param name="A">The repulsing node</param>
        /// <param name="B">The repulsed node</param>
        /// <param name="k">The modulator</param>
        /// <returns></returns>
        private Vector3 RepulsiveForce(N A, N B, float k)
        {
            var d = B.Position - A.Position;
            return d.magnitude == 0 ? Random.onUnitSphere * Random.Range(1f, 1000) : d.normalized * k * k / d.magnitude;
        }


        /// <summary>
        /// Generates the positions of each vertices of the graph
        /// </summary>
        public void GenerateNodePositions()
        {
            List<N> graphNodes = new List<N>(); // Used to keep a track on the already positioned nodes
            Vector3 pos;

            // Count the vertices of the boundaries
            int polSize = GraphNodes.Count(n => n.IsOuterNode);

            // Build the angle for the regular polygon of order k
            var angle = 2 * Mathf.PI / polSize;

            int k = 1;
            // Generate the position of each nodes
            foreach (var node in GraphNodes)
            {
                if (node.IsOuterNode)
                { // Position the outer nodes on the regular polygon inscribed into a unit circle
                    pos = new Vector3(Mathf.Cos(k * angle), Mathf.Sin(k * angle), 0);
                    k++;
                }
                else
                { // Position all the others at the origin
                    pos = Vector3.zero;
                }

                node.SetPosition(pos);
                graphNodes.Add(node);
            }
        }

        public void DistributeNodesPositions()
        {
            var n = GraphNodes.Count;
            var visualArea = new Rect(screenSafeArea.position, screenSafeArea.size * AreaResolution);
            visualArea.center = screenSafeArea.center;
            var W = visualArea.width;
            var H = visualArea.height;

            var min2W = Camera.main.ScreenToWorldPoint(new Vector3(visualArea.min.x, visualArea.min.y, Camera.main.transform.position.z));
            var max2W = Camera.main.ScreenToWorldPoint(new Vector3(visualArea.max.x, visualArea.max.y, Camera.main.transform.position.z));

            var minWPos = Vector3.Min(min2W, max2W);
            var maxWPos = Vector3.Max(min2W, max2W);


            var size = maxWPos - minWPos;
            var radius = Math.Abs(Math.Min(size.x, size.y)) / 2;

            var outNodeCount = GraphNodes.Count(v => v.IsOuterNode);
            var area = .5f * outNodeCount * radius * radius * Mathf.Sin(2 * Mathf.PI / outNodeCount);
            //var area = Mathf.PI;
            //var area = W * H;
            var areaPerVertex = area / n;
            var factor = Mathf.Sqrt(1 / areaPerVertex);
            var K = Mathf.Sqrt(areaPerVertex);
            var maxPer = GraphNodes.Max(v => v.DistanceToBoundary);

            // First, randomly position each node inside the area
            GenerateNodePositions();
            //return;
            for (int i = 1; i <= IterationCount; i++)
            {
                // Initialize the positions to the origin
                foreach (var node in GraphNodes)
                {
                    node.Force = Vector3.zero;
                }

                // Calculate the attractive for on each edges
                foreach (var edge in GraphEdges)
                {
                    var C = factor * GraphTools.PeriphericInfluence(edge.NodeA, edge.NodeB, PeriphericityModulator, maxPer);

                    var A = maxPer == 0 ? Attraction : Mathf.Lerp(1, Attraction, (maxPer - edge.NodeA.DistanceToBoundary) / maxPer);
                    var F = A * AttractiveForce(edge.NodeA, edge.NodeB, C * K);
                    edge.NodeA.Force -= F;
                    edge.NodeB.Force += F;
                }

                // Move the nodes in the direction of the final force, but restrict it to the area
                foreach (var node in GraphNodes)
                {
                    if (node.IsOuterNode) continue;
                    var pos = node.Position;

                    // The displacement must not be more than the current temperature
                    var t = GraphTools.Cool(i, areaPerVertex);
                    pos += node.Force.normalized * Mathf.Min(node.Force.magnitude, t);
                    
                    node.SetPosition(pos);
                }
            }

            // Update the node positions to spread the nodes on the visual area
            foreach (var node in GraphNodes)
            {
                var pos = node.Position * radius;
                pos.z = Random.value * MaxVerticeHeight / 4;
                node.SetPosition(pos);
            }
        }

        public void Render()
        {
            foreach (var node in GraphNodes)
                node.Render(NodeRenderer, NodesContainer, RenderersScale);

            foreach (var edge in GraphEdges)
                edge.Render(EdgeRenderer, EdgesContainer, RenderersScale);
        }

        public void ScaleRenderers()
        {
            foreach (var node in GraphNodes)
            {
                node.UpdateScale(NodeRenderer.transform.localScale * RenderersScale);
            }

            foreach (var edge in GraphEdges)
            {
                edge.UpdateScale(EdgeRenderer.transform.localScale * RenderersScale);
            }
        }
    }
}