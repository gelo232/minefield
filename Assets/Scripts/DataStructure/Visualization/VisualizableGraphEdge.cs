﻿using KGW.DataStructures.Generic;
using UnityEngine;

namespace KGW.DataStructures
{
    public abstract class VisualizableGraphEdge<T> : GraphEdge<T> where T : GraphNode<T>, IVisualizable
    {
        internal GameObject EdgeRenderer;

        public virtual void Render(GameObject template, Transform container, float renderersScale)
        {
            if (EdgeRenderer != null)
                Object.DestroyImmediate(EdgeRenderer);
            EdgeRenderer = Object.Instantiate(template, container);
            EdgeRenderer.transform.position = (NodeA.Position + NodeB.Position) / 2;
            EdgeRenderer.transform.up = (NodeB.Position - NodeA.Position).normalized;
            UpdateScale(template.transform.localScale * renderersScale);
            EdgeRenderer.name = "Edge " + NodeA.name + "_" + NodeB.name;
        }

        protected VisualizableGraphEdge():base(){}
        protected VisualizableGraphEdge(T a, T b):base(a, b)
        {
        }

        internal override void Cleanup()
        {
            if (EdgeRenderer != null)
                Object.DestroyImmediate(EdgeRenderer);
            base.Cleanup();
        }

        public void UpdateScale(Vector3 scale)
        {
            var s = EdgeRenderer.transform.localScale;
            s.x = scale.x;
            s.z = scale.z;
            s.y = Vector3.Distance(NodeA.Position, NodeB.Position);
            EdgeRenderer.transform.localScale = s;
        }
    }
}