﻿using KGW.DataStructures.Generic;
using UnityEngine;

namespace KGW.DataStructures
{
    public abstract class VisualizableGraphNode<T> : GraphNode<T>, IVisualizable where T: VisualizableGraphNode<T>
    {
        internal GameObject NodeRenderer;
        public override void SetPosition(Vector3 newPos)
        {
            if (NodeRenderer != null)
                NodeRenderer.transform.localPosition = newPos;
        }

        public void Render(GameObject template, Transform container, float renderersScale)
        {
            if (NodeRenderer != null)
                DestroyImmediate(NodeRenderer);
            transform.SetParent(container);
            NodeRenderer = Instantiate(template, transform);
            NodeRenderer.transform.position = Position;
            NodeRenderer.name = name + " Renderer";
            UpdateScale(template.transform.localScale * renderersScale);
        }

        internal override void Cleanup()
        {
            if (NodeRenderer != null)
                DestroyImmediate(NodeRenderer);
            base.Cleanup();
        }

        public void UpdateScale(Vector3 scale)
        {
            NodeRenderer.transform.localScale = scale;
        }
    }
}