﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace KGW.MineField
{
    [CustomEditor(typeof(MineFieldGraphManager))]
    public class MineFieldGraphManagerEditor : Editor
    {
        private MineFieldGraphManager manager;

        private GUIStyle groupLabelStyle;
        private GUIStyle groupSubLabelStyle;

        private SerializedProperty verticesCount;
        private SerializedProperty connectionCount;
        private SerializedProperty randomPolyhedralOrder;
        private SerializedProperty polyhedralOrder;
        private SerializedProperty periphericityModulator;
        private SerializedProperty iterationCount;

        private SerializedProperty seed;
        private SerializedProperty maxVerticeHeight;
        private SerializedProperty graphAreaRadius;
        private SerializedProperty areaResolution;
        private SerializedProperty attractionFactor;
        private SerializedProperty repulsionFactor;

        private SerializedProperty nodesLayer;
        private SerializedProperty nodeRenderer;
        private SerializedProperty edgeRenderer;
        private SerializedProperty renderersScale;

        private SerializedProperty minesCount;
        private SerializedProperty nodeDefaultColor;
        private SerializedProperty nodeMineColor;
        private SerializedProperty nodeCleanColor;

        public override VisualElement CreateInspectorGUI()
        {
            groupLabelStyle = new GUIStyle(EditorStyles.largeLabel)
            {
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleCenter
            };
            groupSubLabelStyle = new GUIStyle(EditorStyles.label)
            {
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.UpperLeft
            };

            verticesCount = serializedObject.FindProperty("VerticesCount");
            connectionCount = serializedObject.FindProperty("ConnectionCount");
            randomPolyhedralOrder = serializedObject.FindProperty("RandomPolyhedralOrder");
            polyhedralOrder = serializedObject.FindProperty("PolyhedralOrder");
            periphericityModulator = serializedObject.FindProperty("PeriphericityModulator");
            iterationCount = serializedObject.FindProperty("IterationCount");

            seed = serializedObject.FindProperty("Seed");
            maxVerticeHeight = serializedObject.FindProperty("MaxVerticeHeight");
            //graphAreaRadius = serializedObject.FindProperty("GraphAreaRadius");
            areaResolution = serializedObject.FindProperty("AreaResolution");
            attractionFactor = serializedObject.FindProperty("Attraction");
            repulsionFactor = serializedObject.FindProperty("Repulsion");

            nodesLayer = serializedObject.FindProperty("NodesLayer");
            nodeRenderer = serializedObject.FindProperty("NodeRenderer");
            edgeRenderer = serializedObject.FindProperty("EdgeRenderer");
            renderersScale = serializedObject.FindProperty("RenderersScale");

            minesCount = serializedObject.FindProperty("MinesCount");
            nodeDefaultColor = serializedObject.FindProperty("NodeDefaultColor");
            nodeMineColor = serializedObject.FindProperty("NodeMineColor");
            nodeCleanColor = serializedObject.FindProperty("NodeCleanColor");

            return base.CreateInspectorGUI();
        }

        public override void OnInspectorGUI()
        {
            manager = (MineFieldGraphManager)target;

            EditorGUI.BeginChangeCheck();
            {
                DoGlobalParameters();
                EditorGUILayout.Space();
                DoGraphParameters();
            }
            if (EditorGUI.EndChangeCheck() && Application.isPlaying)
                manager.SetupMineField();

            EditorGUILayout.Space();
            DoGraphVisualization();

            EditorGUILayout.Space();
            DoMineFieldParameter();
        }

        private void DoGlobalParameters()
        {
            EditorGUILayout.BeginVertical("box");
            {
                EditorGUILayout.LabelField(new GUIContent("Global Parameters"), groupLabelStyle);
                EditorGUILayout.PropertyField(seed, new GUIContent("Seed (Randomizer)", "The seed to deterministically randomize the calculations."));
            }
            EditorGUILayout.EndVertical();
        }

        private void DoGraphParameters()
        {
            EditorGUILayout.BeginVertical("box");
            {
                EditorGUILayout.LabelField(new GUIContent("Graph Generation Parameters"), groupLabelStyle);     
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.PropertyField(connectionCount, new GUIContent("Vertices Degree", "The number of connection every node should have. It could be more or less, depending on the other graph parameters. \nMust be strictly less than Vertices Count."));
                EditorGUI.EndDisabledGroup();
                EditorGUILayout.PropertyField(verticesCount, new GUIContent("Vertices Count", "The number of nodes to generate."));
                
                EditorGUILayout.BeginVertical("box");
                {
                    EditorGUILayout.PropertyField(randomPolyhedralOrder, new GUIContent("Randomize Polyhedral Degree", "Whether the polyhedral degree should be randomly generated or not"));
                    EditorGUI.BeginDisabledGroup(randomPolyhedralOrder.boolValue);
                    EditorGUILayout.PropertyField(polyhedralOrder, new GUIContent("Polyhedral Degree", "The number of vertices that the outer polygon should have. \nMust be less than, or equal to Vertices Count."));
                    EditorGUI.EndDisabledGroup();
                }
                EditorGUILayout.EndVertical();
                
                EditorGUILayout.PropertyField(periphericityModulator, new GUIContent("Peripherical Modifier", "The constant that is used to modulate the calculation of the peripherical influence."));
                EditorGUILayout.PropertyField(iterationCount, new GUIContent("Iteration Count", "The number of iteration to perform for calculating the stability factors."));
            }
            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();

            connectionCount.intValue = Math.Min(connectionCount.intValue, verticesCount.intValue - 1); // There must not be more connection per vertex than available vertices + 1
            polyhedralOrder.intValue = Math.Min(polyhedralOrder.intValue, verticesCount.intValue); // There must not be more bounding vertices than available vertices
        }
        
        private void DoGraphVisualization()
        {
            EditorGUILayout.BeginVertical("box");
            {
                EditorGUILayout.LabelField(new GUIContent("Graph Visualization"), groupLabelStyle);
                EditorGUILayout.BeginVertical("box");
                {
                    EditorGUILayout.LabelField(new GUIContent("Position"), groupSubLabelStyle);
                    EditorGUI.BeginChangeCheck();
                    {
                        EditorGUILayout.PropertyField(maxVerticeHeight, new GUIContent("Maximum Depth", "The maximum depth of the vertices of the graph."));
                        //EditorGUILayout.PropertyField(graphAreaRadius, new GUIContent("Area Radius", "The radius of the area that the graph will use."));
                        EditorGUILayout.PropertyField(areaResolution, new GUIContent("Area Resolution", "The percentage of the screen resolution to use for visualizing the graph."));
                        EditorGUILayout.PropertyField(attractionFactor, new GUIContent("Attraction Force", "The force exerted by each edge to pull the nodes together."));
                        //EditorGUILayout.PropertyField(repulsionFactor, new GUIContent("Repulsion Force", "The force exerted by each node to push other nodes away."));
                    }
                    if (EditorGUI.EndChangeCheck() && Application.isPlaying)
                        manager.VisualizeGraph();
                }EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical("box");
                {
                    EditorGUILayout.LabelField(new GUIContent("Rendering"), groupSubLabelStyle);
                    EditorGUILayout.PropertyField(nodesLayer, new GUIContent("Nodes Layer Mask", "The layer mask where the nodes are located."));

                    EditorGUI.BeginChangeCheck();
                    {
                        EditorGUILayout.PropertyField(nodeRenderer, new GUIContent("Node Prefab", "The prefab to use for rendering the nodes."));
                        EditorGUILayout.PropertyField(edgeRenderer, new GUIContent("Edge Prefab", "The prefab to use for rendering the edges."));
                    }
                    if (EditorGUI.EndChangeCheck() && Application.isPlaying)
                        manager.Render();

                    EditorGUI.BeginChangeCheck();
                    EditorGUILayout.PropertyField(renderersScale, new GUIContent("Renderers Scale", "The scale of the vertex and edge renderers."));
                    if (EditorGUI.EndChangeCheck() && Application.isPlaying)
                        manager.ScaleRenderers();
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }

        private void DoMineFieldParameter()
        {
            EditorGUILayout.BeginVertical("box");
            {
                EditorGUILayout.LabelField(new GUIContent("Mine Field Parameters"), groupLabelStyle);

                EditorGUI.BeginChangeCheck();
                {
                    EditorGUILayout.PropertyField(minesCount, new GUIContent("Mines Count", "The total number of mine to place on the area. \nMust be less than, or equal to Vertices Count."));
                }if (EditorGUI.EndChangeCheck() && Application.isPlaying)
                    manager.DistributeMines(true);

                EditorGUILayout.BeginVertical("box");
                {
                    EditorGUILayout.LabelField(new GUIContent("Rendering"), groupSubLabelStyle);
                    EditorGUILayout.PropertyField(nodeDefaultColor, new GUIContent("Unrevealed Color", "The color of unrevealed nodes."));
                    EditorGUILayout.PropertyField(nodeMineColor, new GUIContent("Mined Color", "The color of the revealed nodes that contains a mine."));
                    EditorGUILayout.PropertyField(nodeCleanColor, new GUIContent("Clean Color", "The color of the revealed nodes that do not contain any mine."));
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
            minesCount.intValue = Math.Min(minesCount.intValue, verticesCount.intValue); // There must not be more mines than available nodes
        }
    }
}