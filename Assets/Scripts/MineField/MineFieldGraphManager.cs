﻿using System.Linq;
using UnityEngine;

namespace KGW.MineField
{
    public class MineFieldGraphManager : VisualizableGraphManager<MineFieldGraphManager, MineFieldEdge, MineFieldNode>
    {
        [Range(0, 100)]
        public int MinesCount;

        public Color NodeDefaultColor = Color.gray;
        public Color NodeMineColor = Color.red;
        public Color NodeCleanColor = Color.green;
        public LayerMask NodesLayer;
        public bool HasUnrevealedMine => GraphNodes.Count > 0 && GraphNodes.Any(n => !n.IsRevealed && n.HasMine);
        public bool HasUnrevealedCleanNode => GraphNodes.Count > 0 && GraphNodes.Any(n => !n.IsRevealed && !n.HasMine);

        public delegate void OnNodeSelected_t(MineFieldNode node);
        public delegate void OnMineRevealed_t(MineFieldNode node);

        public OnNodeSelected_t OnNodeSelected;
        public OnMineRevealed_t OnMineRevealed;

        protected override void Start()
        {
            base.Start();

            //SetupMineField();
        }

        void Update()
        {
            if (GameLogic.Instance.IsPlaying && Input.GetKeyUp(KeyCode.Mouse0))
            {
                var selectedNode = GetSelectedNode();
                if (selectedNode != null)
                    HandleNodeSelection(selectedNode);
            }
        }

        public void SetupMineField()
        {
            Random.InitState(Seed);
            Cleanup();
            BuildGraph();
            DistributeMines();
            VisualizeGraph();
        }

        private void HandleNodeSelection(MineFieldNode selectedNode)
        {
            var shouldNotify = !selectedNode.IsRevealed;
            if(selectedNode != null && !selectedNode.IsRevealed)
                selectedNode.Reveal(true);

            if(shouldNotify)
                OnNodeSelected?.Invoke(selectedNode);
        }

        private MineFieldNode GetSelectedNode()
        {
            MineFieldNode res = null;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hit, Mathf.Infinity, NodesLayer))
                res = hit.collider.gameObject.GetComponentInParent<MineFieldNode>();

            return res;
        }

        public void DistributeMines(bool initSeed = false)
        {
            if(initSeed)
                Random.InitState(Seed);

            foreach (var node in GraphNodes)
                node.SetMine(false);

            int remainingMines = MinesCount;
            int i;
            while (remainingMines > 0)
            {
                i = Random.Range(0, GraphNodes.Count);
                if (!GraphNodes[i].HasMine)
                {
                    GraphNodes[i].SetMine(true);
                    remainingMines--;
                }
            }
        }

        internal void MineRevealed(MineFieldNode minedNode) => OnMineRevealed?.Invoke(minedNode);
    }
}