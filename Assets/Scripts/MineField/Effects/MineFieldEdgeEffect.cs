﻿using System.Collections;
using UnityEngine;

namespace KGW.MineField
{
    public class MineFieldEdgeEffect : MineFieldElementEffect
    {
        [HideInInspector] public Vector3 StartPosition;
        [HideInInspector] public Vector3 EndPosition;

        public override void Open()
        {
            for (int i = 0; i < mainModules.Count; i++)
            {
                var module = mainModules[i];
                module.MainModule.maxParticles = module.MaxParticles;
            }
            AudioEffect.volume = maxVolume;

            base.Open();

            StartCoroutine(OpenEffects());
        }


        private IEnumerator OpenEffects()
        {
            var t = 0f;
            while (t <= OpeningDuration)
            {
                var u = t / OpeningDuration;

                transform.position = Vector3.Lerp(StartPosition, EndPosition, u);
                
                if (t < OpeningDuration && t + Time.deltaTime > OpeningDuration)
                    t = OpeningDuration;
                else
                    t += Time.deltaTime;

                yield return null;
            }

            IsOpen = true;
            yield return StartCoroutine(CloseEffects());
        }

        public override void Close()
        {
            StartCoroutine(CloseEffects());
        }

        private IEnumerator CloseEffects()
        {
            var t = 0f;
            while (t <= ClosingDuration)
            {
                var u = t / ClosingDuration;
                for (int i = 0; i < mainModules.Count; i++)
                {
                    var module = mainModules[i];
                    module.MainModule.maxParticles = (int)Mathf.Lerp(module.MaxParticles, 0, u);
                }
                AudioEffect.volume = Mathf.Lerp(maxVolume, 0, u);

                if (t < ClosingDuration && t + Time.deltaTime > ClosingDuration)
                    t = ClosingDuration;
                else
                    t += Time.deltaTime;

                yield return null;
            }

            ParticleEffect?.gameObject.SetActive(false);
            AudioEffect?.gameObject.SetActive(false);
            transform.position = Vector3.zero;

            IsOpen = false;
        }
    }
}