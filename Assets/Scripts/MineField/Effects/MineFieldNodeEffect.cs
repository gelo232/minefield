﻿using System.Collections;
using UnityEngine;

namespace KGW.MineField
{
    public class MineFieldNodeEffect : MineFieldElementEffect
    {
        public GameObject MineObject;
        public enum EffectType{Clean, Mined}

        public EffectType Type = EffectType.Clean;

        void Start()
        {
            if(MineObject != null)
                MineObject.SetActive(false);
        } 

        public override void Open()
        {
            base.Open();

            IsOpen = true;
        }

        public void RevealMine()
        {
            if (MineObject != null)
                MineObject.SetActive(true);
        }

        public override void Close()
        {
            StartCoroutine(CloseEffects());
        }

        private IEnumerator CloseEffects()
        {
            var t = 0f;
            while (t <= ClosingDuration)
            {
                var u = t / ClosingDuration;
                for(int i = 0; i < mainModules.Count; i++)
                {
                    var module = mainModules[i];
                    module.MainModule.maxParticles = (int)Mathf.Lerp(module.MaxParticles, 0, u);
                }
                AudioEffect.volume = Mathf.Lerp(maxVolume, 0, u);

                if (t < ClosingDuration && t + Time.deltaTime > ClosingDuration)
                    t = ClosingDuration;
                else
                    t += Time.deltaTime;

                yield return null;
            }

            ParticleEffect?.gameObject.SetActive(false);
            AudioEffect?.gameObject.SetActive(false);
            IsOpen = false;
        }
    }
}