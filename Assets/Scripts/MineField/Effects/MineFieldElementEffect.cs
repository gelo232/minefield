﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace KGW.MineField
{
    public abstract class MineFieldElementEffect : MonoBehaviour
    {
        protected struct ParticleEffectData
        {
            public ParticleSystem.MainModule MainModule;
            public int MaxParticles;
        }
        public ParticleSystem ParticleEffect;
        public AudioSource AudioEffect;
        public float OpeningDuration = 1;
        public float ClosingDuration = 1;
        public bool LoopEffects;

        public bool IsOpen { get; protected set; }
        
        protected float maxVolume;

        protected List<ParticleEffectData> mainModules = new List<ParticleEffectData>();

        protected virtual void Awake()
        {
            ParticleEffect?.gameObject.SetActive(false);
            AudioEffect?.gameObject.SetActive(false);
            mainModules = ParticleEffect.GetComponentsInChildren<ParticleSystem>(true).Select(p=>new ParticleEffectData(){MainModule = p.main, MaxParticles = p.main.maxParticles}).ToList();
            maxVolume = AudioEffect.volume;
        }

        public virtual void Open()
        {
            ParticleEffect?.gameObject.SetActive(true);
            ParticleEffect?.Play();

            AudioEffect?.gameObject.SetActive(true);
            AudioEffect?.Play();
            
            AudioEffect.loop = LoopEffects;
        }
        public abstract void Close();
    }
}