﻿using System;
using System.Collections;
using System.Linq;
using KGW.DataStructures;
using UnityEngine;

namespace KGW.MineField
{
    public class MineFieldNode : VisualizableGraphNode<MineFieldNode>
    {
        public override Vector3 Position => transform.position;

        private MineFieldNodeEffect revealedSafeEffect;
        private MineFieldNodeEffect revealedMinedEffect;

        public override void SetPosition(Vector3 newPos)
        { 
            transform.position = newPos;
            base.SetPosition(newPos);
        }

        public bool HasMine { get; private set; }
        public bool IsRevealed { get; private set; }
        
        public void SetMine(bool mine)
        {
            HasMine = mine;
        }

        public void Reveal(bool burstIfMined = false)
        {
            if (revealedSafeEffect == null)
                return;
            if (IsRevealed)
                return;
            IsRevealed = true;

            if(HasMine && burstIfMined)
                revealedMinedEffect.Open();
            else
                revealedSafeEffect.Open();
            
            if(HasMine)
            {
                revealedMinedEffect.RevealMine();
                NotifyMineRevealed(this);
            }

            foreach (var node in Neighbors.Where(nb => nb.Neighbors.All(nb2 => !nb2.HasMine)))
                StartCoroutine(RevealDelayed(node));
        }

        private IEnumerator RevealDelayed(MineFieldNode node)
        {
            if (node.IsRevealed)
                yield break;
            if (adjacentEdges.TryGetValue(node, out var edge))
            {
                var mfEdge = (MineFieldEdge) edge;
                mfEdge.Reveal();
                while (!mfEdge.IsRevealed)
                    yield return null;
            }

            node.Reveal();
        }

        public override void NotifyMineRevealed(MineFieldNode node)
        {
            base.NotifyMineRevealed(node);
            MineFieldGraphManager.Instance.MineRevealed(node);
        }

        void Update()
        {
            if(NodeRenderer != null)
            {
                if(revealedSafeEffect == null)
                    revealedSafeEffect = NodeRenderer.GetComponentsInChildren<MineFieldNodeEffect>(true).FirstOrDefault(n=>n.Type == MineFieldNodeEffect.EffectType.Clean);
                
                if (revealedMinedEffect == null)
                    revealedMinedEffect = NodeRenderer.GetComponentsInChildren<MineFieldNodeEffect>(true).FirstOrDefault(n => n.Type == MineFieldNodeEffect.EffectType.Mined);
                
                foreach (var rd in NodeRenderer.GetComponentsInChildren<Renderer>(true))
                {
                    if (rd.gameObject.GetComponent<ParticleSystem>() != null) continue;
                    rd.material.color =
                        !IsRevealed ? MineFieldGraphManager.Instance.NodeDefaultColor :
                        HasMine ? MineFieldGraphManager.Instance.NodeMineColor :
                        MineFieldGraphManager.Instance.NodeCleanColor;
                }
            }
        }

    }
}