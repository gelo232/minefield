﻿using KGW.DataStructures;
using System;
using System.Linq;
using UnityEngine;

namespace KGW.MineField
{
    [Serializable]
    public class MineFieldEdge : VisualizableGraphEdge<MineFieldNode>
    {
        private MineFieldEdgeEffect revealedEffect;

        public bool IsRevealed => revealedEffect != null && revealedEffect.IsOpen;

        public MineFieldEdge():base(){}
        public MineFieldEdge(MineFieldNode a, MineFieldNode b) :base(a, b){}

        public void Reveal()
        {
            if(revealedEffect != null)
            {
                revealedEffect.StartPosition = (NodeA.IsRevealed ? NodeA : NodeB).Position;
                revealedEffect.EndPosition = (!NodeA.IsRevealed ? NodeA : NodeB).Position;
                revealedEffect?.Open();
            }
        }

        public override void Render(GameObject template, Transform container, float renderersScale)
        {
            base.Render(template, container, renderersScale);
            if (EdgeRenderer != null)
            {
                if (revealedEffect == null)
                    revealedEffect = EdgeRenderer.GetComponentInChildren<MineFieldEdgeEffect>();
            }
        }
    }
}