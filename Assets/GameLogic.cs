﻿using System;
using System.Collections;
using System.Collections.Generic;
using KGW.DataStructures.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace KGW.MineField
{
    public class GameLogic : Singleton<GameLogic>
    {
        [Header("Menus")]
        public GameObject StartMenu;
        public GameObject RetryMenu;
        public GameObject SuccessMenu;
        public TextMeshProUGUI RemainingMinesText;

        [Header("Sounds")]
        public AudioSource BackgroundSound;
        //public AudioSource PlaySound;
        public AudioSource FailSound;
        public AudioSource SuccessSound;

        [Header("Level Parameters")]
        [Range(1, 10)]
        public int MaxLevelDifficulty = 10;
        [Range(4, 120)]
        public int BaseDifficultyNodesCount = 12;
        [Range(4, 10)]
        public int BasePolyhedralDegree = 4;
        [Range(1, 119)]
        public int BaseMinesCount = 3;

        private int currentLevel = 0;
        private bool hasFailed;
        private int currentRevealedMinesCount = 0;
        public bool IsPlaying { get; private set; }

        void Start()
        {
            BackgroundSound.Play();
            BackgroundSound.loop = true;

            StartMenu.SetActive(true);
            RetryMenu.SetActive(false);
            SuccessMenu.SetActive(false);

            MineFieldGraphManager.Instance.OnNodeSelected += OnNodeSelected;
            MineFieldGraphManager.Instance.OnMineRevealed += OnMineRevealed;
        }

        private void OnMineRevealed(MineFieldNode node)
        {
            currentRevealedMinesCount++;
            RemainingMinesText.text = (MineFieldGraphManager.Instance.MinesCount - currentRevealedMinesCount).ToString();
        }

        private void OnNodeSelected(MineFieldNode node)
        {
            if(node.HasMine)
                Failure();
        }

        public void BeginPlaying()
        {
            currentLevel = Math.Max(1, (currentLevel + 1)%MaxLevelDifficulty);
            StartLevel();
            //Play();
        }

        private void Play()
        {
            hasFailed = false;
            MineFieldGraphManager.Instance.Seed = Random.Range(0, 100);
            MineFieldGraphManager.Instance.SetupMineField();
            //BackgroundSound.Stop();
            FailSound.Stop();
            SuccessSound.Stop();

            BackgroundSound.loop = true;
            if (!BackgroundSound.isPlaying)
                BackgroundSound.Play();

            StartMenu.SetActive(false);
            RetryMenu.SetActive(false);
            SuccessMenu.SetActive(false);

            currentRevealedMinesCount = 0;
            RemainingMinesText.transform.parent.gameObject.SetActive(true);
            RemainingMinesText.text = (MineFieldGraphManager.Instance.MinesCount - currentRevealedMinesCount).ToString();

            IsPlaying = true;
        }

        public void StartLevel()
        {
            MineFieldGraphManager.Instance.VerticesCount = BaseDifficultyNodesCount * currentLevel;
            MineFieldGraphManager.Instance.PolyhedralOrder = Mathf.Clamp(BasePolyhedralDegree + currentLevel - 1, 4, MineFieldGraphManager.Instance.VerticesCount);
            MineFieldGraphManager.Instance.MinesCount = Mathf.Clamp(BaseMinesCount * currentLevel, 1, MineFieldGraphManager.Instance.VerticesCount - 1);

            Play();
        }

        public void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        public void MainMenu()
        {
            IsPlaying = false;
            hasFailed = false;

            MineFieldGraphManager.Instance.Cleanup();
            currentLevel = 0;

            BackgroundSound.loop = true;
            if(!BackgroundSound.isPlaying)
                BackgroundSound.Play();

            SuccessSound.Stop();
            FailSound.Stop();

            StartMenu.SetActive(true);
            RetryMenu.SetActive(false);
            SuccessMenu.SetActive(false);
            RemainingMinesText.transform.parent.gameObject.SetActive(false);
        }

        public void Failure()
        {
            hasFailed = true;
            IsPlaying = false;
            BackgroundSound.Stop();
            //PlaySound.Stop();
            SuccessSound.Stop();

            if (!FailSound.isPlaying)
                FailSound.Play();

            StartMenu.SetActive(false);
            RetryMenu.SetActive(true);
            SuccessMenu.SetActive(false);
        }

        public void Success()
        {
            IsPlaying = false;
            hasFailed = false;
            BackgroundSound.Stop();
            //PlaySound.Stop();
            if (!SuccessSound.isPlaying)
                SuccessSound.Play();
            FailSound.Stop();

            StartMenu.SetActive(false);
            RetryMenu.SetActive(false);
            SuccessMenu.SetActive(true);
        }

        // Update is called once per frame
        void Update()
        {
            if(IsPlaying && !hasFailed && (!MineFieldGraphManager.Instance.HasUnrevealedCleanNode || !MineFieldGraphManager.Instance.HasUnrevealedMine) )
                Success();
        }
    }
}